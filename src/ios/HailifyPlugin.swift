//
//  HailifyFleetPlugin.swift
//  HelloWorld
//
//  Created by Chirag Patel on 02/08/21.
//

import UIKit
import HailifyFleetSDK
import CoreLocation

@objc(HailifyPlugin) class HailifyPlugin: CDVPlugin, FleetSDKDelegate {

    // MARK: - define callback IDs
    var strCallListnerId : String?
    var strNavigationListnerId : String?
    var strOrderStatusListnerId : String?
    var strBatchCompleteListnerId : String?
    
    // MARK: - initialize and setup methods
    @objc(initialize:)
    func initialize(command: CDVInvokedUrlCommand) {
        var pluginResult = CDVPluginResult (status: CDVCommandStatus_ERROR);

        let key = command.arguments[1] as? String ?? ""

        if !key.isEmpty {
            FleetSDK.shared.platformType = 1
            FleetSDK.shared.initilize(withKey: key)
            pluginResult = CDVPluginResult(status: CDVCommandStatus_OK)
        }
        
        self.commandDelegate!.send(pluginResult, callbackId: command.callbackId); // Send the function result back to cordova.
    }
    
    @objc(setDriverId:)
    func setDriverId(command: CDVInvokedUrlCommand) {
        var pluginResult = CDVPluginResult (status: CDVCommandStatus_ERROR);

        let driverID = command.arguments[0] as? String ?? ""
        if !driverID.isEmpty {
            FleetSDK.shared.setDriverID(driverID)
            pluginResult = CDVPluginResult(status: CDVCommandStatus_OK)
        }
        
        self.commandDelegate!.send(pluginResult, callbackId: command.callbackId); // Send the function result back to cordova.
    }
    
    @objc(enableSandbox:)
    func enableSandbox(command: CDVInvokedUrlCommand) {

        let isSandbox = command.arguments[0] as? Bool ?? false
        FleetSDK.shared.enableSandbox = isSandbox
        let pluginResult = CDVPluginResult(status: CDVCommandStatus_OK)
        
        self.commandDelegate!.send(pluginResult, callbackId: command.callbackId); // Send the function result back to cordova.
    }
    
    @objc(useSDKLocationEngine:)
    func useSDKLocationEngine(command: CDVInvokedUrlCommand) {

        let useSDKLocation = command.arguments[0] as? Bool ?? false
        FleetSDK.shared.useSDKLocationEngine = useSDKLocation
        let pluginResult = CDVPluginResult(status: CDVCommandStatus_OK)
        
        self.commandDelegate!.send(pluginResult, callbackId: command.callbackId); // Send the function result back to cordova.
    }
    
    @objc(showOrderPopup:)
    func showOrderPopup(command: CDVInvokedUrlCommand) {
        var pluginResult = CDVPluginResult (status: CDVCommandStatus_ERROR);

        if self.viewController != nil {
            FleetSDK.shared.showOrderPopup(viewController: self.viewController)
            pluginResult = CDVPluginResult(status: CDVCommandStatus_OK)
            
        }
        
        self.commandDelegate!.send(pluginResult, callbackId: command.callbackId); // Send the function result back to cordova.
    }
    
    @objc(initializeDeliveryView:)
    func initializeDeliveryView(command: CDVInvokedUrlCommand) {
        
        FleetSDK.shared.delegate = self
        
        if self.viewController != nil {
            FleetSDK.shared.showOrderPopup(viewController: self.viewController)            
        }
        
        let pluginResult = CDVPluginResult(status: CDVCommandStatus_OK)
        
        self.commandDelegate!.send(pluginResult, callbackId: command.callbackId); // Send the function result back to cordova.
    }
    
    @objc(setLocation:)
    func setLocation(command: CDVInvokedUrlCommand) {

        let lat = command.arguments[0] as? Double ?? 0.0
        let lng = command.arguments[1] as? Double ?? 0.0
        let location = CLLocation(latitude: lat, longitude: lng)
        
        FleetSDK.shared.setLocation(location: location)
        let pluginResult = CDVPluginResult(status: CDVCommandStatus_OK)
                    
        self.commandDelegate!.send(pluginResult, callbackId: command.callbackId); // Send the function result back to cordova.
    }
    
    @objc(cancelOrder:)
    func cancelOrder(command: CDVInvokedUrlCommand) {
        var pluginResult = CDVPluginResult (status: CDVCommandStatus_ERROR);

        let aditionalResponse = command.arguments[0] as? String ?? ""
        if !aditionalResponse.isEmpty {
            FleetSDK.shared.cancelOrder(aditionalResponse: aditionalResponse)
            pluginResult = CDVPluginResult(status: CDVCommandStatus_OK)
        }
        
        self.commandDelegate!.send(pluginResult, callbackId: command.callbackId); // Send the function result back to cordova.
    }
    
    @objc(setMaxHeightPercentage:)
    func setMaxHeightPercentage(command: CDVInvokedUrlCommand) {

        let percentage = command.arguments[0] as? UInt ?? 100
        
        FleetSDK.shared.maxHeightPercentage = percentage
        let pluginResult = CDVPluginResult(status: CDVCommandStatus_OK)
                    
        self.commandDelegate!.send(pluginResult, callbackId: command.callbackId); // Send the function result back to cordova.
    }
    
    // MARK: - Add Listener
    @objc(setCallListener:)
    func setCallListener(command: CDVInvokedUrlCommand) {
        self.strCallListnerId = command.callbackId
    }
    
    @objc(setCompleteListener:)
    func setCompleteListener(command: CDVInvokedUrlCommand) {
        self.strBatchCompleteListnerId = command.callbackId
    }
    
    @objc(setOrderStatusListener:)
    func setOrderStatusListener(command: CDVInvokedUrlCommand) {
        self.strOrderStatusListnerId = command.callbackId
    }
    
    @objc(setNavigationListener:)
    func setNavigationListener(command: CDVInvokedUrlCommand) {
        self.strNavigationListnerId = command.callbackId
    }
    
    @objc(getCurrentOrderStatus:)
    func getCurrentOrderStatus(command: CDVInvokedUrlCommand) {
        let orderCurrentStatus : String = FleetSDK.shared.getOrderCurrentStatus()
        let pluginResult = CDVPluginResult(status: CDVCommandStatus_OK, messageAs: orderCurrentStatus)
        self.commandDelegate!.send(pluginResult, callbackId: command.callbackId)
    }
    
    // MARK: - FleetSDK Delegate 
    func navigateToLocation(latitude: Double, longitude: Double) {
        //print("FROM FLEET SDK GET LATITUDE : \(latitude) AND LONGITUDE : \(longitude)")
        
        if self.strNavigationListnerId != nil {
            let pluginResult = CDVPluginResult(status: CDVCommandStatus_OK, messageAs: ["latitude" : latitude, "logitude" : longitude])
            pluginResult?.setKeepCallbackAs(true)
            self.commandDelegate!.send(pluginResult, callbackId: self.strNavigationListnerId)
        }
        else {
            GlobalMethods.navigateToMapApp(latitude: latitude, longitude: longitude)
        }
    }
 
    func callToLocation(phoneNumber: String) {
        //print("FROM FLEET SDK GET NUMBER : \(phoneNumber)")
        
        if self.strCallListnerId != nil {
            let pluginResult = CDVPluginResult(status: CDVCommandStatus_OK, messageAs: phoneNumber)
            pluginResult?.setKeepCallbackAs(true)
            self.commandDelegate!.send(pluginResult, callbackId: self.strCallListnerId);
        }
        else {
            phoneNumber.makeAPhoneCall()
        }
    }

    func updatedOrderStatus(deliveryID: String, orderStatus: String) {
        //print("FROM FLEET SDK GET STATUS : \(orderStatus)")
        
        if self.strOrderStatusListnerId != nil {
            let pluginResult = CDVPluginResult(status: CDVCommandStatus_OK, messageAs: ["deliveryID" : deliveryID, "orderStatus" : orderStatus])
            pluginResult?.setKeepCallbackAs(true)
            self.commandDelegate!.send(pluginResult, callbackId: self.strOrderStatusListnerId);
        }
    }

    func batchOrderCompleted() {
        //print("FROM FLEET SDK GET JOB COMLETED")
        
        if self.strBatchCompleteListnerId != nil {
            let pluginResult = CDVPluginResult(status: CDVCommandStatus_OK, messageAs: "FROM FLEET SDK GET JOB COMLETED")
            pluginResult?.setKeepCallbackAs(true)
            self.commandDelegate!.send(pluginResult, callbackId: self.strBatchCompleteListnerId);
        }
    }

}
