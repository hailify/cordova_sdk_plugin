/*
       Licensed to the Apache Software Foundation (ASF) under one
       or more contributor license agreements.  See the NOTICE file
       distributed with this work for additional information
       regarding copyright ownership.  The ASF licenses this file
       to you under the Apache License, Version 2.0 (the
       "License"); you may not use this file except in compliance
       with the License.  You may obtain a copy of the License at

         http://www.apache.org/licenses/LICENSE-2.0

       Unless required by applicable law or agreed to in writing,
       software distributed under the License is distributed on an
       "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
       KIND, either express or implied.  See the License for the
       specific language governing permissions and limitations
       under the License.
*/
package org.apache.cordova.hailify;

import android.location.Location;
import android.provider.Settings;
import android.util.Log;
import android.widget.FrameLayout;

import com.hailify.fleetsdk.DeliveryView;
import com.hailify.fleetsdk.FleetSdk;
import com.hailify.fleetsdk.constants.PlatformType;

import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaInterface;
import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.CordovaWebView;
import org.apache.cordova.PluginResult;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.TimeZone;

public class HailifyPlugin extends CordovaPlugin {
    public static final String TAG = "HailifyPlugin";
    public static String uuid;                                // Device UUID

    private static final String ANDROID_PLATFORM = "Android";
    private static final String AMAZON_PLATFORM = "amazon-fireos";
    private static final String AMAZON_DEVICE = "Amazon";
    private static final String ERR_DELIVERY_VIEW = "First initialization init the delivery view";
    private DeliveryView deliveryView;

    /**
     * Constructor.
     */
    public HailifyPlugin() {
    }

    /**
     * Sets the context of the Command. This can then be used to do things like
     * get file paths associated with the Activity.
     *
     * @param cordova The context of the main Activity.
     * @param webView The CordovaWebView Cordova is running in.
     */
    public void initialize(CordovaInterface cordova, CordovaWebView webView) {
        super.initialize(cordova, webView);
        HailifyPlugin.uuid = getUuid();
    }

    /**
     * Executes the request and returns PluginResult.
     *
     * @param action          The action to execute.
     * @param args            JSONArry of arguments for the plugin.
     * @param callbackContext The callback id used when calling back into JavaScript.
     * @return True if the action was valid, false if not.
     */
    public boolean execute(String action, JSONArray args, CallbackContext callbackContext) throws JSONException {
        if ("initialize".equals(action)) {
            // Toast.makeText(cordova.getContext(), args.toString(), Toast.LENGTH_SHORT).show();
            FleetSdk.INSTANCE.initilize(
                    cordova.getContext().getApplicationContext(),
                    args.getString(0),
                    args.getString(1)
            );
            FleetSdk.INSTANCE.setPlatformType(1);
            if (callbackContext != null) {
                callbackContext.success();
            }
        } else if ("setDriverId".equals(action)) {
            // Toast.makeText(cordova.getContext(), args.toString(), Toast.LENGTH_SHORT).show();
            FleetSdk.INSTANCE.setDriverID(
                    args.getString(0)
            );
        } else if ("enableSandbox".equals(action)) {
            // Toast.makeText(cordova.getContext(), args.toString(), Toast.LENGTH_SHORT).show();
            FleetSdk.INSTANCE.setEnableSandbox(
                    args.getBoolean(0)
            );
        } else if ("setLocation".equals(action)) {
            // Toast.makeText(cordova.getContext(), args.toString(), Toast.LENGTH_SHORT).show();
            Location targetLocation = new Location("");//provider name is unnecessary
            targetLocation.setLatitude(args.getDouble(0));//your coords of course
            targetLocation.setLongitude(args.getDouble(1));
            targetLocation.setAccuracy((float) args.getDouble(2));
            FleetSdk.INSTANCE.setLocation(targetLocation);
        } else if ("initializeDeliveryView".equals(action)) {
            cordova.getActivity().runOnUiThread(new Runnable() {

                @Override
                public void run() {
                    FrameLayout frameLayout = (FrameLayout) webView.getView().getParent();

                    deliveryView = new DeliveryView(cordova.getActivity());
                    FrameLayout.LayoutParams deliveryParam = new FrameLayout.LayoutParams(
                            FrameLayout.LayoutParams.MATCH_PARENT,
                            FrameLayout.LayoutParams.MATCH_PARENT
                    );
                    deliveryParam.topMargin = 400;
                    deliveryView.setLayoutParams(deliveryParam);
                    frameLayout.addView(deliveryView);

                    deliveryView.init(cordova.getActivity());
                    callbackContext.success();
                }
            });
        } else if ("setNavigationListener".equals(action)) {
            if (deliveryView != null) {
                deliveryView.setNavigationListener((lat, lng) -> {
                    try {
                        JSONObject latLngJson = new JSONObject();
                        latLngJson.put("Lat", lat);
                        latLngJson.put("Lng", lng);
//                        callbackContext.success(latLngJson);
                        PluginResult resultA = new PluginResult(PluginResult.Status.OK, latLngJson);
                        resultA.setKeepCallback(true);
                        callbackContext.sendPluginResult(resultA);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                });
                Log.d(TAG, "execute: setNavigationListener -  finish");
            } else {
                callbackContext.error(ERR_DELIVERY_VIEW);
            }
        } else if ("setOrderStatusListener".equals(action)) {
            if (deliveryView != null) {
                deliveryView.setOrderStatusListener((deliveryID, orderStatus) -> {
                    try {
                        JSONObject object = new JSONObject();
                        object.put("deliveryID", deliveryID);
                        object.put("orderStatus", orderStatus);
//                        callbackContext.success(object);
                        PluginResult resultA = new PluginResult(PluginResult.Status.OK, object);
                        resultA.setKeepCallback(true);
                        callbackContext.sendPluginResult(resultA);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                });
                Log.d(TAG, "execute: setOrderStatusListener -  finish");
            } else {
                callbackContext.error(ERR_DELIVERY_VIEW);
            }
        } else if ("setCompleteListener".equals(action)) {
            if (deliveryView != null) {
                deliveryView.setCompleteListener(() -> {
//                    callbackContext.success();
                    PluginResult resultA = new PluginResult(PluginResult.Status.OK);
                    resultA.setKeepCallback(true);
                    callbackContext.sendPluginResult(resultA);
                });
                Log.d(TAG, "execute: setCompleteListener -  finish");
            } else {
                callbackContext.error(ERR_DELIVERY_VIEW);
            }
        } else if ("setCallListener".equals(action)) {
            if (deliveryView != null) {
                deliveryView.setCallListener(phoneNumber -> {
                    try {
                        JSONObject object = new JSONObject();
                        object.put("phoneNumber", phoneNumber);
//                        callbackContext.success(object);
                        PluginResult resultA = new PluginResult(PluginResult.Status.OK, object);
                        resultA.setKeepCallback(true);
                        callbackContext.sendPluginResult(resultA);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                });
                Log.d(TAG, "execute: setCallListener -  finish");
            } else {
                callbackContext.error(ERR_DELIVERY_VIEW);
            }
        } else if ("showOrderPopup".equals(action)) {
            if (deliveryView != null) {
                deliveryView.showOrderPopup();
                callbackContext.success();
                Log.d(TAG, "execute: showOrderPopup -  finish");
            } else {
                callbackContext.error(ERR_DELIVERY_VIEW);
            }
        } else if ("onResume".equals(action)) {
            if (deliveryView != null) {
                cordova.getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        deliveryView.onResume();
                        //                callbackContext.success("Success");
                        PluginResult resultA = new PluginResult(PluginResult.Status.OK, "Success");
                        resultA.setKeepCallback(true);
                        callbackContext.sendPluginResult(resultA);
                        Log.d(TAG, "execute: showOrderPopup -  finish");
                    }
                });
            } else {
                callbackContext.error(ERR_DELIVERY_VIEW);
            }
        } else if ("onDestroy".equals(action)) {
            if (deliveryView != null) {
                deliveryView.onDestroy();
//                callbackContext.success("Success");
                PluginResult resultA = new PluginResult(PluginResult.Status.OK, "Success");
                resultA.setKeepCallback(true);
                callbackContext.sendPluginResult(resultA);
                Log.d(TAG, "execute: showOrderPopup -  finish");
            } else {
                callbackContext.error(ERR_DELIVERY_VIEW);
            }
        } else if ("useSDKLocationEngine".equals(action)) {
            boolean flg = args.getBoolean(0);
            FleetSdk.INSTANCE.setUseSDKLocationEngine(flg);
        } else if ("cancelOrder".equals(action)) {
            String additionalResponse = args.getString(0);
            FleetSdk.INSTANCE.cancelOrder(additionalResponse);
        } else if ("getCurrentOrderStatus".equals(action)) {
            String currentOrderStatus = FleetSdk.INSTANCE.getCurrentOrderStatus();
            try {
                JSONObject object = new JSONObject();
                object.put("currentOrderStatus", currentOrderStatus);
                PluginResult resultA = new PluginResult(PluginResult.Status.OK, object);
                resultA.setKeepCallback(true);
                callbackContext.sendPluginResult(resultA);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else if ("setMaxHeightPercentage".equals(action)) {
            if (deliveryView != null) {
                float percentage = (float) args.getDouble(0);
                deliveryView.setMaxHeightPercentage(percentage);
                Log.d(TAG, "execute: setMaxHeightPercentage -  finish");
            }
        } else {
            return false;
        }
        return true;
    }

    //--------------------------------------------------------------------------
    // LOCAL METHODS
    //--------------------------------------------------------------------------

    /**
     * Get the OS name.
     *
     * @return
     */
    public String getPlatform() {
        String platform;
        if (isAmazonDevice()) {
            platform = AMAZON_PLATFORM;
        } else {
            platform = ANDROID_PLATFORM;
        }
        return platform;
    }

    /**
     * Get the device's Universally Unique Identifier (UUID).
     *
     * @return
     */
    public String getUuid() {
        String uuid = Settings.Secure.getString(this.cordova.getActivity().getContentResolver(), android.provider.Settings.Secure.ANDROID_ID);
        return uuid;
    }

    public String getModel() {
        String model = android.os.Build.MODEL;
        return model;
    }

    public String getProductName() {
        String productname = android.os.Build.PRODUCT;
        return productname;
    }

    public String getManufacturer() {
        String manufacturer = android.os.Build.MANUFACTURER;
        return manufacturer;
    }

    public String getSerialNumber() {
        String serial = android.os.Build.SERIAL;
        return serial;
    }

    /**
     * Get the OS version.
     *
     * @return
     */
    public String getOSVersion() {
        String osversion = android.os.Build.VERSION.RELEASE;
        return osversion;
    }

    public String getSDKVersion() {
        @SuppressWarnings("deprecation")
        String sdkversion = android.os.Build.VERSION.SDK;
        return sdkversion;
    }

    public String getTimeZoneID() {
        TimeZone tz = TimeZone.getDefault();
        return (tz.getID());
    }

    /**
     * Function to check if the device is manufactured by Amazon
     *
     * @return
     */
    public boolean isAmazonDevice() {
        return android.os.Build.MANUFACTURER.equals(AMAZON_DEVICE);
    }

    public boolean isVirtual() {
        return android.os.Build.FINGERPRINT.contains("generic") ||
                android.os.Build.PRODUCT.contains("sdk");
    }

}