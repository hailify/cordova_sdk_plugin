// var argscheck = require('cordova/argscheck');
var channel = require("cordova/channel");
// var utils = require('cordova/utils');
var exec = require("cordova/exec");
var cordova = require("cordova");

var HailifyPlugin = {
  getInfo: function (callback) {
    exec(callback, null, "HailifyPlugin", "getMyPluginInfo", null);
  },
  onResume: function (callback, errorCallback) {
    cordova.exec(callback, errorCallback, "HailifyPlugin", "onResume", null);
  },
  onDestroy: function (callback, errorCallback) {
    cordova.exec(callback, errorCallback, "HailifyPlugin", "onDestroy", null);
  },
  showOrderPopup: function (callback = null, errorCallback) {
    cordova.exec(callback, errorCallback, "HailifyPlugin", "showOrderPopup", null);
  },
  cancelOrder: function (options) {
    cordova.exec(null, null, "HailifyPlugin", "cancelOrder", [
      options,
    ]);
  },
  initialize: (options, callback = null) => {
    cordova.exec(callback, null, "HailifyPlugin", "initialize", [
      options.fleetName,
      options.fleetToken,
    ]);
  },
  setDriverId: (driverId, callback = null) => {
    cordova.exec(callback, null, "HailifyPlugin", "setDriverId", [driverId]);
  },
  setLocation: (options, callback = null) => {
    cordova.exec(callback, null, "HailifyPlugin", "setLocation", [
      options.latitude,
      options.longitude,
      options.accuracy,
    ]);
  },
  enableSandbox: (isEnableSandbox, callback = null) => {
    cordova.exec(callback, null, "HailifyPlugin", "enableSandbox", [isEnableSandbox]);
  },

  useSDKLocationEngine: (locationEngine, callback = null) => {
    cordova.exec(callback, null, "HailifyPlugin", "useSDKLocationEngine", [locationEngine]);
  },
  initializeDeliveryView: (callback) => {
    cordova.exec(
      callback,
      null,
      "HailifyPlugin",
      "initializeDeliveryView",
      null
    );
  },
  setNavigationListener: (callback) => {
    cordova.exec(
      callback,
      null,
      "HailifyPlugin",
      "setNavigationListener",
      null
    );
  },
  setOrderStatusListener: (callback) => {
    cordova.exec(
      callback,
      null,
      "HailifyPlugin",
      "setOrderStatusListener",
      null
    );
  },
  setCompleteListener: (callback) => {
    cordova.exec(callback, null, "HailifyPlugin", "setCompleteListener", null);
  },
  setCallListener: (callback) => {
    cordova.exec(callback, null, "HailifyPlugin", "setCallListener", null);
  },
  getCurrentOrderStatus: (callback) => {
    cordova.exec(
      callback,
      null,
      "HailifyPlugin",
      "getCurrentOrderStatus",
      null
    );
  },
  setMaxHeightPercentage: (options) => {
    cordova.exec(null, null, "HailifyPlugin", "setMaxHeightPercentage", [options.percentage]);
  }
};

module.exports = HailifyPlugin;
