# Hailify-FleetSDK

## Description.

By Hailify

- [cordova-plugin-hailify](#cordova-plugin-hailify)
- [Installation](#installation)
- [Properties](#properties)
- [Supported Platforms](#supported-platforms)
- [Quick Example](#quick-example)

### Supported Platforms

- Android [Deployed on Android 6(marshmalllow) or higher]
- iOS [Deployed on iOS 13 or higher]

# cordova-plugin-hailify

This plugin defines a global `Hailify` object, which describes the all other methods which is used for the integrate Hailify job delivery feature inside client application.

Although the object is in the global scope, it is not available until after the `deviceready` event.

```js
document.addEventListener("deviceready", onDeviceReady, false);
function onDeviceReady() {
  // All other Hailify method which is accessible by use of "Hailify" Object
}
```

# Installation

- Execute below command in terminal to install this plugin.

```
    cordova plugin add https://bitbucket.org/hailify/cordova_sdk_plugin.git --variable HALIFIY_SDK_USER_NAME="<your_hailify_userid>" --variable HALIFIY_SDK_PASSWORD="<your_hailify_userid_password>"
```
`HALIFIY_SDK_USER_NAME`, and `HALIFIY_SDK_PASSWORD` will be provided by the hailify support team.

# Properties

- [Initialize SDK](#initialize-sdk)
- [Set DriverId](#set-driverid)
- [Show order popup](#show-order-popup)
- [Enable Sandbox](#enable-sandbox)
- [Use SDK location Engine](#use-sdk-location-engine)
- [Set Location Manually](#set-location-manually)
- [Initialize Delivery View](#initialize-delivery-view)
- [Navigation Listener](#navigation-listener)
- [Order Status Listener](#order-status-listener)
- [Order Complete Listener](#order-complete-listener)
- [Call Listener](#call-listener)
- [Cancel Order](#cancel-order)
- [Get order Status](#get-order-status)
- [Resume callback](#resume-callback)
- [Destroy callback](#destroy-callback)

## initialize SDK

`Hailify.initialize({ fleetName:<your_fleet_name>, fleetToken:<your_fleet_token> },[callback])`

- initialize a Hailify SDK shared instance with key/token

```Note:- Hailify will provide sandbox/production SDK key/token.```

- fleetName: Name of your company

- fleetToken: Token which provided by hailify to you.

- callback (Optional): success callback when the SDK initialization completed successfully.

e.g:

```js
  Hailify.initialize({
      fleetName:'Name',
      fleetToken:'Token' }, 
      () => {
        // you will get callback success here.
      }
  )
```

## Set DriverId

`Hailify.setDriverId(driverId)`

- driverId: ID of your driver who is registered with your organization.

e. g.: 
```
Hailify.setDriverId('a123')
```

## Show order popup

`Hailify.showOrderPopup(callback, errorCallback)`

 - App should call this method once you got success response from Assign Driver end-point(AssignBooking)

e.g:
```js
  Hailify.showOrderPopup(()=>{
      // Your job delivery popup is visible to the user.
  }, (error)=>{
      // If any error in showing job delivery popup then it will visible here.
  })
```

## Enable Sandbox

`Hailify.enableSandbox(true/false)`

- By default it is `false`
- Enable sandbox environment in order to test orders.

e. g.: 
```
Hailify.enableSandbox(true)
```

## Use SDK location Engine

`Hailify.useSDKLocationEngine(true/false)`

- By default it is `false`.
- Allow Hailify SDK to use SDK Location engine.

e.g.: 
```
Hailify.useSDKLocationEngine(true)
```

## Set Location Manually

```
Hailify.useSDKLocationEngine(false)
Hailify.setLocation({ latitude: number, longitude: number, accuracy: number })
```

- Or provide your location from your app to Hailify SDK with calling above method.

e.g. :
```
Hailify.useSDKLocationEngine(false)
Hailify.setLocation({ 
    latitude: 40.0, 
    longitude: -74.0, 
    accuracy: 5 
})
```

## Initialize Delivery View

`Hailify.initializeDeliveryView(callback)`

- For adding the Delivery View inside your cordova application you have to call this method and after successful initialize of Delivery view, you will get a success callback. then you can set other listeners which is related to Delivery view and can receive order updates after initialize from SDK.

e.g.:
```js
  Hailify.initializeDeliveryView(()=>{
    // All listners will be set here.
  })
```

## Navigation Listener (Optional)

`Hailify.setNavigationListener(callback)`

- Customize navigation option for the current order. To fetch current order latitude & longitude, Add following listener method. If this listener will not defined then Hailify SDK opens route from current location to Order location in Apple Maps or Google Maps based on what is installed in the device.
- This is optional method.

e.g: 

Note: `You Have to set this listener after the successful initilization of the delivery view.`

```js
  Hailify.initializeDeliveryView(()=>{
    Hailify.setNavigationListener((data)=>{
        // data.Lat, data.Lng json will be shown here.
    })
  })
```

## Order Status Listener (Optional)

`Hailify.setOrderStatusListener(callback)`

- Define following listener method If you want to get updated status of current order. This listener callback will call when the current order status will change. Possible values: Check Delivery Statuses
- This is optional method.

e.g: 

Note: `You Have to set this listener after the successful initilization of the delivery view.`

```js
  Hailify.initializeDeliveryView(()=>{
    Hailify.setOrderStatusListener((data)=>{
        // data.deliveryID, data.orderStatus json will be shown here.
    })
  })
```


## Order Complete Listener (Optional)

`Hailify.setCompleteListener(callback)`

- Following listener method will call just after you will complete the current booking.
- This is optional method.

e.g:

Note: `You Have to set this listener after the successful initilization of the delivery view.`

```js
  Hailify.initializeDeliveryView(()=>{
    Hailify.setCompleteListener((data)=>{
        // result ok will return.
    })
  })
```


## Call Listener (Optional)

`Hailify.setCallListener(callback)`

- Customize call option for the current order location(either pick-up or drop-off). Add following listener method in order to get phone number of pick-up or drop-off location. If this method will not defined then Hailify SDK call to the current order’s location from the device.
- This is optional method.

e.g: 

Note: `You Have to set this listener after the successful initilization of the delivery view.`

```js
  Hailify.initializeDeliveryView(()=>{
    Hailify.setCallListener((data)=>{
        // data.phoneNumber will return
    })
  })
```

## Cancel Order (Optional)

`Hailify.cancelOrder(aditionalResponse)`

- Invoke to Hailify SDK when any order get cancelled. Called below method with additional response when Hailify cancel order via calling end-point(CancelOrder).

 - You must pass aditionalResponse value in this method. 

Note: `You will get aditionalResponse in end-point request (CancelOrder) `

e.g.: 
```js
Hailify.cancelOrder('This order has been canceled by distributer')
```

## Get order Status (Optional)

`Hailify.getCurrentOrderStatus(callback)`

- Get order current status at any time. Possible values: Check Delivery Statuses

e.g.: 
```js
  Hailify.getCurrentOrderStatus((data)=>{
      // data.currentOrderStatus will return
  })
```

## Resume callback (Android only)

`Hailify.onResume(callback, errorCallback)`
- This is necessory for delivery view. so you have to add this in `resume` event.

e.g.:
```js
  // After on device ready callback from the corodova.
  function onResume(){
    Hailify.onResume(()=>{

    }, (error) => {
      
    })
  }
  document.addEventListener("resume", onResume, false);
```
## Destroy callback (Android only)

`Hailify.onDestroy(callback)`

- If you want to remove the delivery view from your application or destroy view when the application closed, then you have to use this method.

e.g:
```js
    Hailify.onDestroy(()=>{

    }, (error) => {
      
    })
```


## Delivery Statuses
| Status           | Description |
| -------------   | ------------- |
| booked         | delivery is successfully created and courier en-route soon  |
| to_pickup     | courier actively on his way to pickup location  |
| at_pickup     | courier at pickup location  |
| to_delivery   | courier picked up the order and actively on his way to delivery location  |
| at_delivery   | courier at delivery location  |
| to_return      | courier return in transit to pickup location. (Only Return flow)  |
| returned       | delivery returned  |
| delivered      | delivery completed  |
| cancelled     | delivery cancelled  |
| failed            | the delivery was not complete because of a merchant or customer facing issue  |

### Quick Example

```js

function onDeviceReady() {
    // Cordova is now initialized. Have fun!

    setupSDK()

    console.log('Running cordova-' + cordova.platformId + '@' + cordova.version);
    document.getElementById('deviceready').classList.add('ready');
}

function setupSDK() {

  Hailify.initialize({
    fleetName: "name",
    fleetToken: "token",
  });
  Hailify.setDriverId("driverId");
  Hailify.enableSandbox(true);
  Hailify.useSDKLocationEngine(true);

  //Delivery view initialization and listeners setup.
  Hailify.initializeDeliveryView(() => {
    setOrderStatusListener();
    setNavigationListener();
    setCompleteListener();
    setCallListener();
  });

  document.addEventListener("resume", onResume, false);
}

function onResume() {
  Hailify.onResume((data) => {
    console.log("onResume", data);
  });
}

function setNavigationListener() {
  Hailify.setNavigationListener((data) => {
    alert(JSON.stringify(data));
  });
}

function setOrderStatusListener() {
  Hailify.setOrderStatusListener((data) => {
    alert(JSON.stringify(data));
  });
}

function setCompleteListener() {
  Hailify.setCompleteListener((data) => {
    alert(JSON.stringify(data));
  });
}

function setCallListener() {
  Hailify.setCallListener((data) => {
    alert(JSON.stringify(data));
  });
}

function getCurrentOrderStatus() {
  Hailify.getCurrentOrderStatus((data) => {
    alert(JSON.stringify(data));
  });
}

function showOrderPopup() {
  Hailify.showOrderPopup(null, (error) => {
    alert(error);
  });
}
```
