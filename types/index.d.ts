// Type definitions for cordova-plugin-device 2.0
// Project: https://github.com/apache/cordova-plugin-device
// Definitions by: Microsoft Open Technologies Inc <http://msopentech.com>
//                 Tim Brust <https://github.com/timbru31>
// Definitions: https://github.com/DefinitelyTyped/DefinitelyTyped

/**
 * This plugin defines a global device object, which describes the device's hardware and software.
 * Although the object is in the global scope, it is not available until after the deviceready event.
 */
export interface Hailify {
  onResume: (callback: (message: string) => void) => void;
  onDestroy: (callback: (message: string) => void) => void;
  getInfo: (callback: (data: any) => void) => void;
  initialize: (
    options: {
      fleetName: string;
      fleetToken: string;
    },
    callback?: () => void
  ) => void;
  setDriverId: (
    options: {
      driverId: string;
    },
    callback?: () => void
  ) => void;
  setLocation: (
    options: {
      latitude: number;
      longitude: number;
      accuracy: number;
    },
    callback?: () => void
  ) => void;
  showOrderPopup: (callback: (message: string) => void) => void;
  cancelOrder: (
    options: { additionalResponse: string },
    callback: () => void
  ) => void;
  enableSandbox: (
    options: {
      isEnableSandbox: boolean;
    },
    callback?: () => void
  ) => void;
  useSDKLocationEngine: (
    options: {
      useSDKLocationEngine: boolean;
    },
    callback?: () => void
  ) => void;
  initializeDeliveryView: (callback?: () => void) => void;
  setNavigationListener: (callback: (data: any) => void) => void;
  setOrderStatusListener: (callback: (data: any) => void) => void;
  setCompleteListener: (callback: () => void) => void;
  setCallListener: (callback: (data: any) => void) => void;
  getCurrentOrderStatus: (callback: (data: any) => void) => void;
}

export declare var HailifyPlugin: Hailify;
